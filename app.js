/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , http = require('http')
  , path = require('path')
	, time = require('time');
	
var fs = require('fs');
var app = express();

var current_time = new time.Date();
console.log(current_time.getUTCHours() + ':' + current_time.getUTCMinutes() + ':' + current_time.getUTCSeconds());
setInterval(function(){
	var current_time = new time.Date();
	console.log(current_time.getUTCHours() + ':' + current_time.getUTCMinutes() + ':' + current_time.getUTCSeconds());
}, 5000)

// app.enable("jsonp callback");
app.configure(function(){
  app.set('port', 8080);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  app.use(express.favicon(__dirname + '/public/images/favicon.ico'));
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
	app.use(express.cookieParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});


// Routes

app.post('/rest/:verb', routes.restPost); 
// app.post('/rest/login', routes.restLogin);
// app.post('/rest/register', routes.restRegister);
app.get('/rest/:data', routes.restGet);

var app_server = http.createServer(app).listen(8080, '50.116.38.43', function(){
  console.log("Express server listening on port 8080");
});
