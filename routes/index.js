var customModule = require('custom-module');

exports.restLogin = function(req, res){
	var data = req.body;
	console.log(data);
	customModule.checkPassword(data, function(result, role, id){
		console.log('Password Checked');
		console.log(result);
		console.log(role);
		if (result) {
			customModule.getFirebaseToken(data.username, role, id, function(token) {
				res.setHeader('Cache-Control', 'no-cache');
				res.json(token);	
			});
		}
		else {
			res.setHeader('Cache-Control', 'no-cache');
			res.json(false);
		}
	});
}

exports.restRegister = function(req, res) {
	customModule.addUser(req.body, function(result) {
		res.json(result);
	});
}

exports.restPost = function(req, res){
	res.setHeader('Cache-Control', 'no-cache');
	var data = req.body;
	var verb = req.params.verb;
	if (verb == 'register') {
		customModule.addUser(req.body, function(result) {
			res.json(result);
		});
	}
	if (verb == 'password') {
		customModule.changePassword(req.body, function(result) {
			res.json(result);
		});
	}
	if (verb == 'password-reset') {
		customModule.resetPassword(req.body, function(result) {
			res.json(result);
		});
	}
	if (verb == 'login') {
		var data = req.body;
		console.log(data);
		customModule.checkPassword(data, function(result, role, id){
			console.log('Password Checked');
			console.log(result);
			console.log(role);
			if (result) {
				customModule.getFirebaseToken(data.username, role, id, function(token) {
					res.setHeader('Cache-Control', 'no-cache');
					res.json(token);	
				});
			}
			else {
				res.setHeader('Cache-Control', 'no-cache');
				res.json(false);
			}
		});
	}
};

exports.restGet = function(req, res){
	res.setHeader('Cache-Control', 'no-cache');
	var path = req.params.data;
	console.log(path);
	res.json(path);
}

// Cookies come in req.cookies.NAME
// if (req.cookies != undefined) var cookie = req.cookies.session;
// // 	else var cookie = undefined;

